
1. User Interface

	A. Debug Interface

		1. Low-Level Test Interface

			The debug interface shall provide a simple means with which to communicate with the
			glucose reader.  The primary motivation for this interface is to allow a robust low
			level test interface during early development, and to fascilitate design verification
			activities.

			a. Query Hardware Subsystems

				The debug interface shall provide a command interface with which to query the drivers
				of all major hardware subsystems, including but not limited to: USB, SPI, I2C.

					{ "id": "get_usb_state" }
					{ "id": "get_spi_state" }
					{ "id": "get_i2c_state" }

		2. Measurement Test Interface

			The debug interface shall provide a command interface with which to query the following
			glucose reader measurements: diffuse reflectance, patient skin temperature, analyte fluoresence
			intensity.

					{ "id": "get_diff_ref" }
					{ "id": "get_skin_temp" }
					{ "id": "get_glucose_intensity" }
					{ "id": "get_refdye_intensity" }

		3. State Machine Test Interface

			The debug interface shall provide a command interface with which to control the application
			state machine to aid in design verification activities, as well as support general debugging.

					{ "id": "set_state", "state":"new_state" }

					{ "id": "get_state", "state":"new_state" }

		4. Diagnostics Test Interface

			The debug interface shall provide a command interface with which to extract diagnostic information
			such as the state of the watchdog, logs, and all significant fault detection mechanisms.

					{ "id": "get_watchdog" }
					{ "id": "get_logs" }
					{ "id": "get_faults" }

			The debug interface shall provide a command interface with which to enable/disable logging.

					{ "id": "enable_logging" }
					{ "id": "disable_logging" }

			The debug interface shall provide a command interface with which to set the level of detail
			used for logging messages.

					{ "id": "set_logging_level", "level":new_level }

		5. Output Test Interface

			The debug interface shall provide a command interface with which to initiate data streaming in test
			mode, and to manipulate excitation control in test mode.

					{ "id": "enable_test_stream" }
					{ "id": "disable_test_stream" }

					{ "id": "set_excitation_pattern", "pattern":new_pattern }
					{ "id": "get_excitation_pattern" }

	B. Graphical Interface

		1. Stream Measurements

			The graphical interface shall provide a command interface with which to enable/disable data streaming
			to the user interface.

			{ "id": "enable_meas_stream" }
			{ "id": "disable_meas_stream" }

			The graphical interface shall provide a command interface with which to control the content of the data
			stream to the user interface.

			{ "id": "add_diff_ref" }
			{ "id": "add_skin_temp" }
			{ "id": "add_glucose_intensity" }
			{ "id": "add_refdye_intensity" }

			{ "id": "remove_diff_ref" }
			{ "id": "remove_skin_temp" }
			{ "id": "remove_glucose_intensity" }
			{ "id": "remove_refdye_intensity" }

		3. Configuration

			The graphical interface shall provide a command interface with which to configure the glucose reader.

				{ "id": "set_glucose_freq" }
				{ "id": "set_ref_dye_freq" }

				{ "id": "get_glucose_freq" }
				{ "id": "get_ref_dye_freq" }


		4. Calibration

			The graphical interface shall provide a command interface with which to the glucose reader.

2. Control

	A. State Machine

		1. Command Handler

			The command handler shall accept commands from both debug and the graphical interface.

			The command handler shall process all commands in less than TBD ms.

		2. Configuration


		3. Event Generation


		4. Analyte Calculation


		5. Signal Processing


		6. States And Transitions

			The state machine shall implement the following states: idle, busy, error, test.

			The initial of the state machine shall be the idle state.

			In the idle state, the state machine shall wait to receive a valid command, and immediately
			discard ill-formed, or invalid commands. 


	B. Stiumulus Module

		1. Diffuse Reflectance Measurement
		2. Analyte Measurement
		3. External Patient Measurements

	C. Diagnostic Module

		1. Logging
		2. Watchdog
		3. Fault Detection

3. Output

	A. Hard Deadlines

		1. Excitation Control

	B. Soft Deadlines

		1. Data Streaming

4. Communication

	A. Data Format

	{ "id": "cmd_id", "params": ["param1","param2","param3","param4"] }

